function [z] = calcZ(Ht, u2, g, beta2b, Qt, D2, b2, delta2, lambda2)

A1 = delta2 / (pi*D2) * sqrt(1+( tan(beta2b)^-1 / sin(lambda2) )^2);
A2 = u2 / g * Qt / (pi*D2*b2*tan(beta2b));
A3 = u2^2 / g * pi * sin(beta2b);
A4 = u2^2 / g;

a = A1 * Ht - A1 * A4 - A3 * A1^2;
b = A4 - Ht + 2*A1 * A3 - A2;
c = -1*A3;

z = (-b + sqrt(b.^2 - 4 .* a .* c)) ./ (2.*a);
function [fi,n] = findInt(v)
% Find the integer between the values stored in v.
% Some assumptions are made here:
% The value stored in v is sorted.
%
% v  - the vector stores the values to be processed, column vector
% fi - the integers found
% n  - the number of found values, it is the same of length(fi), -1 on error

n = -1;

% check the input argument

[row, col] = size(v);
if (col ~= 1)
    % This is an error
    fprintf('Error: the values should all be positive.\n');
end

if (row == 1)
    % This is an error
    fprintf('Error: not enough values.\n');
end

sumPositive = sum(v <= 0);

if (sumPositive ~= 0)
    % This is an error
    fprintf('Error: the values should all be positive.\n');
end

minV = min(v);
maxV = max(v);

intMinV = ceil(minV);
intMaxV = floor(maxV);

fi = intMinV:1:intMaxV;

n = length(fi);
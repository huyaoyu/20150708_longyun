
% Description
% ===========
%
% Draw the z vs. Ht curve.
%
% The use should specify the range of Ht by setting the values of Ht_1 and
% Ht_2. Note that Ht_1 > Ht_2.
%
% The values of the other paramters are set by the associated arguments.
% 
% Author
% ======
%
% Yaoyu Hu <huyaoyu@sjtu.edu.cn> (Bug report)
%
% Date
% ====
%
% Created: 2015-07-08
%


% ================ clear the workspace ===============

close all; % clean the workspace
clear all;
clc;

% =============== User specified variables =============

Ht_1 = 90;
Ht_2 = 150;

g = 9.8; % the gravitational acceleration

n       = 1480;          % angular speed, rpm
D2      = 0.743;         %
b2      = 0.225;         %
beta2b  = 23 / 180 * pi; %
Qt      = 4.9683;        % flow rate, m^3/s 
delta2  = 0.01;          %
lambda2 = 90 / 180 * pi; %

% =================== Constants ===================

Ht_step = 0.1;

FONT_SIZE_LABEL = 14;
FONT_SIZE_TITLE = 16;
FONT_SIZE_TICK  = 14;
FONT_NAME = 'Times New Rome';

LINE_WIDTH = 2;

NORM_LIMIT = 1e-8;

% ================= Input chcek =================

% Not implemented.

% =================== Calculation ================

Ht = Ht_1:Ht_step:Ht_2;
Ht = Ht';

u2 = pi * n * D2 / 60;

% calculate z
z = calcZ(Ht, u2, g, beta2b, Qt, D2, b2, delta2, lambda2);

plot(z, Ht, 'LineWidth',LINE_WIDTH);
xlabel('Ht','FontName',FONT_NAME,'FontSize',FONT_SIZE_LABEL);
ylabel('z','FontName',FONT_NAME,'FontSize',FONT_SIZE_LABEL);
title('z vs. H_t','FontName',FONT_NAME,'FontSize',FONT_SIZE_TITLE);

% check the results
Ht_z = calcHt(z, u2, g, beta2b, Qt, D2, b2, delta2, lambda2);

nHtZ = norm(Ht_z - Ht);

if (nHtZ < NORM_LIMIT)
    fprintf('norm = %f\n',nHtZ);
else
    fprintf('Possible failure of z calculation!\nThe norm is %f.\n',nHtZ);
end

% find the integer z

[intZ,nIntZ] = findInt(z);
if (nIntZ > 0)
    Ht_intZ = calcHt(intZ, u2, g, beta2b, Qt, D2, b2, delta2, lambda2);
    fprintf('The possible z values are:\n');
    fprintf('z   Ht\n');
    for I = 1:1:nIntZ
        fprintf('%d\t%f\n',intZ(I),Ht_intZ(I));
    end
else
    fprintf('No valid z values are found\n');
end
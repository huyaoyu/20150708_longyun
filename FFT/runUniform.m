
%
% Author
% ======
%
% Yaoyu Hu <huyaoyu@sjtu.edu.cn>
%
% Date
% ====
%
% Created: 2015-09-02
%

% ================ Clear the workspace ==================

clear all;
close all;
clc;

% ==================== Local settings ===================

iFileName   = 'uniform.xlsx';
oFileName   = 'uniform_extracted.xlsx';
sheetNumber = 1;
range       = 'A1:CAQ901';

selectedColumns = {
    'Monitor Point: DYdown1 (Pressure)';
    'Monitor Point: DYdown2 (Pressure)';
    };

% ==================== Read the file ======================

fprintf('Parse data from file \"%s\", read sheet %d, range %s...\n',...
    iFileName, sheetNumber, range);
[num, txt, raw] = xlsread(iFileName, sheetNumber, range);
fprintf('File parsed...\n');

% ===================== Extract columns ====================

nSelectedCols        = size(selectedColumns,1);
nCols                = size(txt,2);
firstColumn          = cell(1,nCols);
for I = 1:1:nCols
    firstColumn{1,I} = txt{1,I};
end % I

idxSelectedCols = zeros(1,nCols);

nColFound = 0;

for I = 1:1:nSelectedCols
    for J = 1:1:nCols
        if (strcmp(selectedColumns{I,1},firstColumn{1,J}) == 1)
            nColFound = nColFound + 1;
            idxSelectedCols(nColFound) = J;
        end
    end % J
end % I

if (nColFound ~= 0)
    fprintf('%d columns seletected, %d columns found in total.\n', nSelectedCols, nColFound);
else
    fprintf('No columns found!\nPlease check the input arguments! Abort now!\n');
    return;
end

firstColumn4Output = cell(1,nColFound);
% firstColumn4Output{1,1} = txt{1,1};
for I = 1:1:nColFound
    firstColumn4Output{1,I} = txt{1,idxSelectedCols(1,I)};
end % I

% ======================= Save the extracted data ======================

nRowNum = size(num,1);

extractedData = zeros(nRowNum, nColFound);

% extractedData(:,1) = num(:,1);

extractedData(:,1:end) = num(:,idxSelectedCols(1,1:nColFound));

% test the existence of the file

if (exist(oFileName, 'file') == 2)
    % delete the file
    delete(oFileName);
end

[status,message] = xlswrite(oFileName,firstColumn4Output,1,'A1');
if (status ~= 1)
    fprintf('xlswrite returns error flag when writing the first row. The message is:\n%s\n',message);
    return;
end
[status,message] = xlswrite(oFileName,extractedData,1,'A2');
if (status ~= 1)
    fprintf('xlswrite returns error flag when writing the data. The message is:\n%s\n',message);
    return;
end
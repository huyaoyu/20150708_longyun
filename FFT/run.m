
%
% Author
% ======
%
% Yaoyu Hu <huyaoyu@sjtu.edu.cn>
%
% Date
% ====
%
% Created: 2015-08-30
%

% ================ Clear the workspace ==================

clear all;
close all;
clc;

% ==================== Local settings ===================

fileName    = 'uniform_extracted.xlsx';
sheetNumber = 1;

sampleFreq = 1/(2.25e-4*5); % The actual sample frequency whent the data is recorded, Hz

figureFileNamePrefix = 'F_FFT_';
resultFileNamePrefix = 'R_FFT_';
resultFileNameExt    = 'dat';

nInterestedRows = 900; % if should not exceed the range described by the range variable.

% ==================== Read the file ======================

fprintf('Parse data from file \"%s\", read sheet %d, range %s...\n',...
    fileName, sheetNumber);
num = xlsread(fileName, sheetNumber);
fprintf('File parsed...\n');

% ========================== FFT ==========================

[row,col] = size(num);

% test the range
if ( row < nInterestedRows)
    % this is an error
    fprintf('The interested rows is out of range.\nThe data range is %d by %d, the interested rows is %d.\n',...
        row, col, nInterestedRows);
    return;
end

% FFT

[ay, freq, phi] = fftAtFreq(num(end-nInterestedRows+1:end,:),sampleFreq, nInterestedRows);

% save the FFT result data
save('ay.dat','ay','-ascii');
colFreq = freq';
save('freq.dat','colFreq','-ascii');

% ======================== Post-process the result ========================

for I = 1:1:col
    
    fprintf('Processing the %dth channel...\n', I);
    
    strNum = sprintf('%0d',I);
    
    h = figure;
    
    plot(freq',ay(:,I));
    title(['FFT of case ',strNum]);
    xlabel('Frequency (Hz)');
    ylabel('Amplitude');
%     ylim([0,1.2]);
    
    saveas(h,[figureFileNamePrefix,strNum],'fig');
    saveas(h,[figureFileNamePrefix,strNum],'png');
end % I
function Ht = calcHt(z, u2, g, beta2b, Qt, D2, b2, delta2, lambda2)

phi2 = 1 - z .* delta2 / (pi*D2) * sqrt( 1 + ( (tan(beta2b)^-1)/sin(lambda2) )^2 );

Ht = u2 / g * ( u2 * (1-pi.*phi2.*sin(beta2b)./z) - Qt ./ (pi.*D2.*b2.*phi2.*tan(beta2b)) );